const input = document.querySelector('#input');
input.addEventListener('input', onInputValueChange);

const display = document.querySelector('#display');

const copyButton = document.querySelector('button#copy');
copyButton.addEventListener('click', onCopyButtonClick);

const snackbar = document.querySelector('#snackbar');

function onInputValueChange(e) {
    updateDisplay(e.target.value);
}

function updateDisplay(str) {
    // Check if input letters are valid
    const inputHasErrors = !isInputValid(str);

    // Update error indicator
    updateErrorIndicator(inputHasErrors);

    // Update value if it is valid
    if (!inputHasErrors) {
        // Construct output from letters
        const output = generateOutput(str);

        // Update & display output
        display.innerHTML = output;
    }
}

function isInputValid(str) {
    const possibleLetters = Object.keys(letters);
    return [...str].every(c => possibleLetters.includes(c));
}

function generateOutput(str, lineSeparator = '<br>') {
    let output = '';
    for (let row = 0; row < letterHeight; ++row) {
        [...str].forEach(c => output += letters[c][row]);
        output += lineSeparator;
    }
    return output;
}

function updateErrorIndicator(showError) {
    if (showError) {
        input.classList.add('error');
    } else {
        input.classList.remove('error');
    }
}

function onCopyButtonClick() {
    navigator.clipboard.writeText(generateOutput(input.value, '\n')).then(
        () => showSnackbar('Copied to clipboard'),
        (err) => showSnackbar('Error copying to clipboard')
    );
}

function showSnackbar(msg) {
    snackbar.innerHTML = msg;
    snackbar.classList.add('visible');
    setTimeout(() => snackbar.classList.remove('visible'), 3000);
}

// Initialize

updateDisplay('');